﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternsLesson2
{
    public class Application
    {
        public Application(Dialog dialog) {
            dialog.Render();
        }
    }
}
