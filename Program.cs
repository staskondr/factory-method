﻿using System;

namespace PatternsLesson2
{
    class Program
    {
        private const string Html = "Html";
        private const string Win = "Win";
        static void Main(string[] args) {
            if (args.Length < 1) {
                throw new ArgumentException($"Incorrect argument length, should be one argument \"{Html}\" or \"{Win}\"");
            }

            Dialog dialog;
            var os = args[0];
            switch (os) {
                case Html:
                    dialog = new HtmlDialog();
                    break;
                case Win:
                    dialog = new WindowsDialog();
                    break;
                default:
                    throw new ArgumentException($"Incorrect argument, should be \"{Html}\" or \"{Win}\"");
            }
            var application = new Application(dialog);
            Console.WriteLine(dialog.GetType());
            Console.WriteLine(dialog.GetType());
            Console.Read();
        }
    }
}
