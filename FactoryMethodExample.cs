﻿namespace PatternsLesson2
{
    public interface IButton
    {
        void Render();

        void OnClick(object handler);
    }

    public class WindowsButton : IButton
    {
        public void Render()
        {
            //render windows button
        }

        public void OnClick(object handler)
        {
            //process on click windows button event
        }
    }

    public class HtmlButton : IButton
    {
        public void Render()
        {
            //render html button
        }

        public void OnClick(object handler) {
            //process on click html button event
        }
    }

    public abstract class Dialog
    {
        public void Render()
        {
            var okButton = CreateButton();
            okButton.OnClick(new object());//some handler
            okButton.Render();
        }

        public abstract IButton CreateButton();
    }

    public class WindowsDialog : Dialog
    {
        public override IButton CreateButton()
        {
            return new WindowsButton();
        }
    }

    public class HtmlDialog : Dialog
    {
        public override IButton CreateButton() {
            return new HtmlButton();
        }
    }
}